<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;

class AdminMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        // Si el role->id del usuario es 2 (encargado) o 3 (administrador) podrán acceder
        if (Auth::check() && (Auth::user()->role->id == '2' || Auth::user()->role->id == '3')) {
            return $next($request);
        } else {
            Auth::logout();
            return redirect()->route('login')->with(['message' => 'No tienes permisos suficientes para acceder a este sitio']);
        }
    }
}
