<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    // Returns to the profile configuration view if is "encargado"
    // Returns to the configuration view if is "user"
    public function myProfile()
    {
        $user = \Auth::user();
        $url = "http://http://127.0.0.1:8000/" . $_SERVER['REQUEST_URI'];
        // Check url so that the user will be redirected to the correct one
        if (strpos($url, 'admin')) {
            return view('user.config', ['user' => $user]);
        } else {
            return view('user.myProfile', ['user' => $user]);
        }
    }

    // 
    public function colaboradores()
    {
        $sql = 'SELECT * FROM users WHERE role_id = 2';
        $colaboradores =  DB::select($sql);
        if (!empty($colaboradores)) {
            return view('user.colaboradores', ['colaboradores' => $colaboradores]);
        } else {
            return view('user.colaboradores')->with('message', 'Todavía no hay colabores disponibles');
        }
    }

    // Update user (encargado & user)
    public function update(Request $request)
    {
        $user = \Auth::user();
        $id = $user->id;

        // Validación del formulario
        $validate = $this->validate($request, [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'nick' => 'required|string|max:255|unique:users,nick,' . $id,
            'email' => 'required|string|email|max:255|unique:users,email,' . $id,
            'background' => 'dimensions: max:3000'
        ]);

        // Asignación de valores
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->nick = $request->nick;
        $user->description = $request->description;

        //Almacenar avatar
        $image = $request->file('image');
        if ($image) {
            //Nombre unico
            $image_full = time() . $image->getClientOriginalName();
            //Almacenar en la carpeta 
            Storage::disk('users')->put($image_full, File::get($image));
            $user->image = $image_full;
        }

        //Almacenar background
        $background = $request->file('background');
        if ($background) {
            //Nombre unico
            $background_full = time() . $background->getClientOriginalName();
            //Almacenar en la carpeta 
            Storage::disk('backgrounds')->put($background_full, File::get($background));
            $user->background = $background_full;
        }
        $user->update();
        return redirect()->back()->with(['message' => 'Usuario actualizado correctamente']);
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'password' => 'required|confirmed'
        ]);
        $user = \Auth::user();
        $user->password = $request->password;

        //Comprobar que no viene nulo
        if (($request->password != null)) {
            $user->password = Hash::make($request->password);
        }
        $user->update();
        return redirect()->back()->with(['message' => 'Usuario actualizado correctamente']);
    }

    // Flyer
    public function getImage($filename)
    {
        $file = Storage::disk('users')->get($filename);
        return new Response($file, 200);
    }

    // Background
    public function getBackground($filename)
    {
        $file = Storage::disk('backgrounds')->get($filename);
        return new Response($file, 200);
    }
}
