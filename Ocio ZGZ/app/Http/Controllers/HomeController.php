<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class HomeController extends Controller
{
    // Returns admin panel home view
    public function admin_home()
    {
        $user = auth()->user();
        return view('admin.welcome', ['user' => $user]);
    }

    // Returns client landing view
    public function client_home()
    {
        $user = auth()->user();
        return view('client.home', ['user' => $user]);
    }
}
