<?php

namespace App\Http\Controllers;

use App\Mail\BookingNotification;
use App\Models\Booking;
use App\Models\Event;
use App\Models\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Mail;

class BookingController extends Controller
{

    // View for making a booking: CLIENT
    public function create($id)
    {
        $event = Event::find($id);
        $bookings = Booking::all();
        return view('client.booking.create', ['bookings' => $bookings, 'event' => $event], ['middleware' => 'auth']);
    }
    
    // Function for booking storage
    public function store(Request $request, $id)
    {
        $event = Event::find($id);
        $booking = new Booking;
        DB::transaction(function () use ($event, $booking, $request) {

            $user = \Auth::user();
            $booking->user_id = \Auth::user()->id;
            $booking->event_id =  $event->id;
            $booking->asistentes = $request->asistentes;
            $booking->price = (($request->asistentes) * ($event->price))+1;

            // Subtract the attendees from the remaining number of the event
            $event->restantes = $event->getRestantes($request->asistentes);
            $event->save();
            $booking->save();
            $encargado = User::find($event->user_id);

            // Sends an email to the owner of the event after a booking had been made
            Mail::to($encargado->email)->send(new BookingNotification($user, $event, $booking, $encargado));

        });

        return redirect('/colaboradores')->with('message', 'Reserva realizada');
    }

    // Show an specific booking: ADMIN PANEL
    public function show($id)
    {
        $booking = Booking::find($id);
        return view('admin.booking.show', ['booking' => $booking]);
        $this->middleware('auth');
    }

     // Show all the bookings an user has made: CLIENT
     public function myBookings() {
        $user = \Auth::user();
        return view('client.booking.listBookings', ['user' => $user]);
        $this->middleware('auth');
    }
}
