<?php

namespace App\Http\Controllers;

use App\Mail\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{

    public function contactUs()
    {
        return view('client.contactUs');
    }

    public function contactUsPost(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'subject' => 'required',
            'email' => 'required|email',
            'msg' => 'required'
        ]);

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'subject' => $request->subject,
            'msg' => $request->msg
        );

        Mail::to('alberto.perales.lafuente@gmail.com')->send(new ContactUs($data));
  
        return back()->with('message', 'Gracias por contactarnos! En cuanto podamos nos pondremos en contacto contigo.');
    }

}
