<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Event;
use App\Models\Booking;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventBookingPolicy
{
    use HandlesAuthorization;

    public function owner(Event $event, Booking $bookings) {
        return $event->id == $bookings->event_id;
    }
}
