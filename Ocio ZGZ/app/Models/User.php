<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'role_id', 'name', 'surname', 'nick', 'email', 'image', 'password', 'background', 'description'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Relaciones
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    // Comprobar si es administrador
    public function isAdministrador() {
        return $this->role()->where('name', 'administrador')->exists();
    }
    // Comprobar si es encargado
    public function isEncargado() {
        return $this->role()->where('name', 'encargado')->exists();
    }

}
