<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'event_id', 'asistentes', 'price', 
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function promotions()
    {
        return $this->hasMany(Promotion::class);
    }
    public function events()
    {
        return $this->belongsTo(Events::class);
    }

    public function getEvent($id) {
        $event = Event::find($id);
        return $event;
    }
}
