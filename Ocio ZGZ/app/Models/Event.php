<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Booking;
use Carbon\Carbon;
use DB;


class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'date', 'max_hour', 'name', 'description', 'location', 'image_path', 'capacity', 'price', 'restantes', 'state'
    ];

    protected $dates = [
        'date', 'max_hour'
    ];
    
    // Checks if event has expired
    public function getStateAttribute($state) {
        $date = \Carbon\Carbon::createFromTimestamp(strtotime($this->date->format('Y-m-d') . $this->max_hour->format('H:i:s')));
        $now = \Carbon\Carbon::now();      
        return $date > $now;
    }

    

    // 
    public function getRestantes($asistentes)
    {
        $current_asistentes = Booking::where('event_id', $this->id)->sum('asistentes'); 
        $new_asistentes = $current_asistentes + $asistentes; 
        $restantes = $this->capacity - $new_asistentes; 

        if ($restantes <= 0) {
            DB::table('events')->where('id', $this->id)->update(['state'=> false]);
            return $restantes = 0;
        }
        
        return $restantes;
    }

    public function checkState()
    {
        if ($this->restantes > 0) {
            return $this->state = true;
        } else {
            return $this->state = false;
        }
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }
}
