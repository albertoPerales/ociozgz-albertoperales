<?php

namespace App\Mail;

use App\Models\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookingNotification extends Mailable
{
    use Queueable, SerializesModels;



    public function __construct($user, $event, $booking, $encargado)
    {
        $this->user = $user;
        $this->event = $event;
        $this->booking = $booking;
        $this->encargado = $encargado;
    }


    public function build()
    {

        return $this->view('admin.mail.booking_notification')
            ->from('ocio-zgz@gmail.com', 'Ocio ZGZ')
            ->subject('Tienes una nueva reserva')
            ->with([
                'user' => $this->user,
                'event' => $this->event,
                'booking' => $this->booking,
                'encargado' => $this->encargado
            ]);
    }
}
