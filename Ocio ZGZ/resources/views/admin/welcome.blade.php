<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
@extends('admin.app')
@section('title', 'Panel de control')
<style>
    #message {
        width: 100%;
        background: rgb(250, 240, 229);
        color: #000;
        padding: 2em;
        border-radius: 10px;
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.2);
        border-collapse: collapse;

    }


    #welcome {
        display: flex;
        flex-direction: column;
        width: 100%;
        height: auto;
        padding-top: 1em;
        padding-bottom: 2em;
    }

    .btn-events {
        margin-top: 1em;
        display: flex;
        flex-direction: row;
        width: 18em;
        padding: 1em;
        justify-content: space-between;
        padding-left: 0px;

    }

    .box {
        background-color: #fff;
        backdrop-filter: blur(8px);
        padding: 2em;
        margin-top: 1em;
        border-radius: 10px;
        transition: ease-in-out 0.3s;
    }

    .atajos-box {
        margin-left: 1em;
        background-color: #fff;
        backdrop-filter: blur(8px);
        padding: 2em;
        margin-top: 1em;
        border-radius: 10px;
        width: auto;
        transition: ease-in-out 0.3s;
        font-size: 1.3em;
        /* overflow: hidden; */

    }

    .atajos-box-item {
        display: flex;
        flex-direction: row;
        align-items: center;
        margin-bottom: 1em;
        margin-right: 1em;
        width: fit-content;
        position: relative;
        transition: ease-in-out 0.2s;

    }

    .atajos-box-item>i {
        margin-right: 1em;
        background: -webkit-linear-gradient(left,
                rgba(255, 93, 177, 1) 0%,
                rgb(255, 149, 29) 100%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
    }

    .atajos-box:hover {
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.2);
        border-collapse: collapse;
    }

    .box:hover {
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.2);
        border-collapse: collapse;
    }

    /*NUBES INFORMATIVAS*/
    #mis-eventos-box,
    #nuevo-evento-box,
    #ver-reservas-box,
    #configurar-perfil-box {
        position: absolute;
        z-index: 5;
        background-color: white;
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.3);
        width: 20em;
        padding: 1em;
        font-size: 0.7em;
        bottom: -40;
        right: -315;
        border-radius: 10px;
        display: none;
    }

    #boxes-container {
        display: flex;
        flex-direction: row;
        width: 100%;
    }

    @media screen and (max-width: 768px) {
        #boxes-container {
            flex-direction: column;
        }

        .box {
            width: 100% !important;
        }

        .atajos-box {
            margin-left: 0;
        }
    }
</style>

<script>
    $(document).ready(function() {
        var w = window.innerWidth;
        $(window).on('resize', function() {
            if (w > 768) {
                animation();
            }
        });
        $(window).on('load', function() {
            if (w > 768) {
                animation();
            }
        });



        /**/
        const MOVIL = 1
        const TABLET = 2
        const ESCRITORIO = 3
        var tipo = 0;

        function main() {
            atarEvento();
        }

        function atarEvento() {
            window.addEventListener("resize", redimensionaVentana, false);
        }

        function redimensionaVentana() {
            var w = window.innerWidth;
            var h = window.innerHeight;
            // document.getElementById("demo").innerHTML = "Width: " + w + "<br>Height: " + h;
            if (w < 540 && tipo != MOVIL) {
                tipo = MOVIL;

                $('#xmenu').hide();
                $('#xmenu').css('display', 'none');
                // alert("Se ha cambiado a modalidad MOVIL");
            } else {
                $('#xmenu').show();
                $('#xmenu').css('display', 'flex');
                // alert('se ha cambiado a otra modalidad');
            }
        }

    });


    function animation() {
        $('.atajos-box-item').on('mouseover', function() {
            $(this).find('div').show();
        });
        $('.atajos-box-item').on('mouseout', function() {
            $(this).find('div').hide();
        });
    }
</script>

<!--Título dinámico-->
@section('content')
<div id="welcome">
    <div id="message">
        <h2 style="color:#fea361">¡Bienvenido {{$user->nick}}!</h2><br>
        <p>Te damos la bienvenida al panel de control de <strong>Ocio ZGZ</strong></p>
    </div>
    <div id="boxes-container">
        <div class=" box" style="position: relative; overflow:hidden; width:40%">
        <div id="blur"></div>
        <div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 10px; width: 100%;  position:absolute; top:0; left:0"></div>
        <h1>¡Bienvenido al panel de control!</h1>
        <p>Puedes usar los links de "Atajos" para navegar rápidamente a las areas más importantes para la gestión de tus eventos. </p>
        <p>Usa el formulario de contacto de la página principal para pedir soporte en caso de ser necesario.</p>
    </div>
    <div class="atajos-box">
        <div>
            <div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 10px; width: 100%;  position:absolute; top:0; left:0"></div>
            <h3>Atajos</h3><br>
            <div id="flex-row" style="display:flex;flex-direction:row; align-items:center; flex-wrap:wrap">
                <ul>
                    <a href="/admin/eventos" class="atajos-box-item">
                        <i class="fa fa-list"></i>
                        <li>Mis eventos</li>
                        <div id="mis-eventos-box">
                            <p>Visualiza todos tus eventos y accede a herramientas para su gestión.</p>
                        </div>
                    </a>
                    <a href="/admin/eventos/create" class="atajos-box-item">
                        <i class="fa fa-plus"></i>
                        <li>Nuevo evento</li>
                        <div id="nuevo-evento-box">
                            <p>Crea un nuevo evento y comienza a recibir reservas.</p>
                        </div>
                    </a>
                    <a href="/admin/total-reservas" class="atajos-box-item">
                        <i class="fa fa-eye"></i>
                        <li>Ver reservas</li>
                        <div id="ver-reservas-box">
                            <p>Visualiza todas las reservas de tus eventos.</p>
                        </div>
                    </a>
                    <a href="/admin/my-profile" class="atajos-box-item">
                        <i class="fa fa-user"></i>
                        <li>Configurar perfil</li>
                        <div id="configurar-perfil-box">
                            <p>Configura tu datos y tu perfil público para generar mayor un impacto en tus clientes.</p>
                        </div>
                    </a>
                </ul>
                <img src="{{asset('img/atajos.jpg')}}" width="200px">
            </div>
        </div>

    </div>
</div>
</div>
</div>
@endsection