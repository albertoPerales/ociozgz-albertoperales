@extends('admin.app')
@section('title', 'Listado total de reservas')
<!--Título dinámico-->
<style>
    table {
        width: 100%;
        border-collapse: collapse;
    }

    th,
    td {
        width: 25%;
        vertical-align: top;
        padding: 0.3em;
        caption-side: bottom;
        border-collapse: collapse;
    }


    td {
        text-align: center;
        border-bottom: 1px solid black;
        padding: 1em;
    }

    thead {
        background: linear-gradient(to left, #ffc62a, #ff62c6);
        color: white;
        font-weight: bold;
    }

    th {
        text-align: center;

    }
</style>

@section('content')
<h1>Listado total de reservas</h1>
<div>
    <table>
        <thead>
            <tr>
                <th>NOMBRE</th>
                <th>EVENTO</th>
                <th>FECHA DEL EVENTO</th>
                <th>Nº ASISTENTES</th>
                <th>IMPORTE</th>
            </tr>
        </thead>
        @foreach($events as $event)
            @foreach ($event->bookings as $booking)
            <tr>
                <td>{{$booking->user->name}} {{$booking->user->surname}}</td>
                <td><a href="/admin/eventos/{{$booking->event_id}}">{{$event->name}}</td>
                <td>{{$event->date->format('d-m-Y')}}</td>
                <td>{{$booking->asistentes}}</td>
                <td>{{$booking->price}}€</td>
            </tr>
            @endforeach
        @endforeach

    </table>
</div>
</div>
@endsection