@extends('admin.app')
@section('title', 'Nuevo evento')

<style>
    input[type="text"],
    input[type="date"],
    input[type="number"],
    input[type="time"] {
        background-color: rgba(229, 229, 229, 0.5);
        height: 2.5em;
        border: none;
        padding: 10px;
    }

    .box {
        background-color: #fff;
        padding: 1em;
        border-radius: 10px;
        transition: ease-in-out 0.3s;
    }

    .box:hover {
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.4);
        border-collapse: collapse;
    }

    .container-form {
        display: flex;
        flex-direction: row;
        width: 70%;
        margin-bottom: 6em;
        border-radius: 10px;
    }

    .form {
        margin: 20px;
        margin-top: 0;
        margin-left: 0;
        display: flex;
        flex-direction: column;
    }

    .error-message {
        color: tomato;
        margin-top: 10px
    }

    .form-checkbox {
        width: 50%;
        margin: 20px;
        margin-left: 0;
    }

    .btn-form {
        width: 100%;
        background: linear-gradient(to left, #ffc62a, #ff62c6);
        color: #fff;
        text-align: center;
        border: none;
        height: 2.5em;
    }

    label {
        margin-bottom: 10px;
    }

    #promotions {
        overflow-y: auto;
        width: 15em;
    }

    #welcome {
        margin-top: 20px;
    }

    @media screen and (min-width: 0) and (max-width: 768px) {

        form,
        .container-form {
            width: 100% !important;
        }
    }
</style>
<!--Título dinámico-->
@section('content')
<div id="welcome" style="margin-top: 20px;">
    <div class="container-form">
        <form action="/admin/eventos" method="post" class="box" enctype="multipart/form-data">
            <h1>Crear Evento<div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 6px; width: 50%; margin-top: 10px; border-radius:20px"></div>
            </h1>
            @csrf
            <div class="form">
                <label for="name"><strong>Nombre<span style="color: tomato;"> *</span></strong></label>
                <input placeholder="Nombre del evento" type="text" name="name" value="{{ old('name') }}">
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>
            <div class="form">
                <label for="description"><strong>Descripción</strong></label>
                <input type="text" placeholder="Descripción del evento" name="description" value="{{ old('description') }}">
            </div>


            <div class="form">
                <label for="date"><strong>¿Para qué día? <span style="color: tomato;"> *</span></strong></label>
                <input type="date" name="date" value="{{ old('date') }}">
                @error('date')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>
            <div class="form">
                <label for="max_hour"><strong>¿Hasta cuánto dura?<span style="color: tomato;"> *</span></strong></label>
                <input type="time" name="max_hour" value="{{ old('max_hour') }}">
                @error('max_hour')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>

            <div class="form" style="width: 100%;">
                <label for="location"><strong>¿Dónde se celebra?<span style="color: tomato;"> *</span></strong></label>
                <input type="text" style="width:100%" placeholder="Localización del evento" name="location" value="{{ old('location') }}">
                @error('location')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>

            <div class="form">
                <label for="capacity"><strong>Capacidad máxima<span style="color: tomato;"> *</span></strong></label>
                <input type="text" placeholder="Capacidad máxima de reservas" name="capacity" value="{{ old('capacity') }}""
                @error('capacity')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>
            <div class="form">
                <label for="price"><strong>Precio<span style="color: tomato;"> *</span></strong></label>
                <input type="number" placeholder="€" name="price" value="{{ old('price') }}">
                @error('price')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>


            <div class="form">
                <label for="image_path"><strong>Flyer</strong></label>
                <input id="file-upload" type="file" name="image_path" value="{{ old('image_path') }}">
                @error('image_path')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>
            <div class="form">
                <div class="form" style="margin-right: 0px; margin-top: 3em">
                    <input id="createEvent" class="btn-form" style="cursor: pointer;" type="submit" value="Crear">
                    <a href="javascript:history.back()" style="width: 100%;
                    background: linear-gradient(to left, #ffc62a, #ff62c6);
                    color: #fff;
                    text-align: center;
                    border: none;
                    margin-top: 10px; padding:0.7em;">Atrás</a>
                </div>
            </div>
    </div>

    </form>
    <script>
    </script>
</div>
@endsection