<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    <title>@yield('title') - Ocio ZGZ</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.css') }}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <style>
        #profile {
            margin-right: 3em;
        }

        .dropdown-menu {
            padding-right: 1em;
            z-index: 9999;
            display: none;
            width: 10em;
            height: auto;
            background-color: #fff;
            position: absolute;
            right: -25;
            top: 45;
            font-size: 0.8em;
            text-align: right;
            font-family: 'Montserrat';
            border-radius: 5px;
            padding-top: 0.5em;
            padding-bottom: 0.5em;
            box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.2);
        }

        .dropdown-menu li {
            width: 100%;
        }

        .dropdown-menu li {
            white-space: nowrap;
            width: 100%;
            padding: 0.5em;
        }

        .dropdown-menu li a:hover {
            color: rgba(0, 0, 0, 0.6);

        }

        .alert-message {
            width: 100%;
            height: 3em;
            background-color: rgb(181, 235, 204);
            border-bottom: 2px solid rgb(43, 159, 92);
            display: flex;
            text-align: center;
            align-items: center;
            margin-bottom: 1em;
            color: rgb(43, 159, 92);
            border-radius: 10px;
        }
    </style>
    <script>

    </script>

    <script>
        $(document).ready(function() {
            $('#dropdownIcon, #nickName').click(function() {
                if ($('#dropdownIcon').attr('class') == 'fa fa-angle-down') {
                    $('#dropdownIcon').removeClass('fa fa-angle-down').addClass('fa fa-angle-up')
                } else {
                    $('#dropdownIcon').removeClass('fa fa-angle-up').addClass('fa fa-angle-down')
                }
                $('.dropdown-menu').toggle();
            });
            $('#close-alert').click(function() {
                $('.alert-message').css('display', 'none');
            });

            // Loader cuando se están cargando las vistas
            loader();
        });
        // tipo = 0 -> sin asignar
        // tipo = 1 -> tamaño menor que 350 px
        // tipo = 2 -> tamaño entre 350 y 700 px
        // tipo = 3 -> tamaño mayor que 700 px
        const MOVIL = 1
        const TABLET = 2
        const ESCRITORIO = 3
        var tipo = 0;

        function main() {
            atarEvento();
            redimensionaVentana();
        }

        function atarEvento() {
            window.addEventListener("resize", redimensionaVentana, false);
        }

        function redimensionaVentana() {
            var w = window.innerWidth;
            var h = window.innerHeight;
            var estado = false;
            // document.getElementById("demo").innerHTML = "Width: " + w + "<br>Height: " + h;
            if (w < 768) {
                tipo = MOVIL;
                $('.nav').hide();
                if ($('#hamburger').click(function() {
                        $('.nav').toggle();
                        estado = true;
                    }));
                // Falla que al abrir y cerrar, si cambio a otro dispositivo, no se muestra el menú

            } else {
                $('.nav').show();
            }
        }


        // Script para cerrar alertas
    </script>
</head>

<body onload="main()">
    <!--ANIMACIÓN DE CARGA-->
    <div class="pre-loader-container" id="loader">
        <div class="pre-loader">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>

    </div>
    <!--VERIFICACIÓN-->

    <div class="container">
        <!--HEADER-->
        <header>
            <!-- <div id="notifications">
                <i class="fa fa-bell" style="font-size: 25px; cursor:pointer; color:#000;"></i>
                <i class="fa fa-angle-down" style="font-size: 20px; margin-left:0.5em; cursor: pointer; color:#000;"></i>
            </div> -->

            <div id="profile">
                <div class="container-avatar">
                    @include('admin.includes.avatar')
                </div>
                <p id="nickName" style="margin-left: 10px; cursor:pointer">{{Auth::user()->nick}}</p>
                <i id="dropdownIcon" class="fa fa-angle-down" style="font-size: 20px; margin-left:0.5em; cursor: pointer; color:#000; position:relative">
                </i>
                <div class="dropdown-menu">
                    <i class="fa fa-caret-up" style="position: absolute; right:25; top:-17;color:white; font-size:1.8em"></i>
                    <li><a href="{{route('config')}}">Mi perfil</a></li>
                    <h4 id="demo"></h4>
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Cerrar sesión') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </div>
            </div>

        </header>

        <!--ASIDE-->
        <aside>
            <div id="logo">
                <a href="/">
                    <img src="{{ asset('img/logo.svg') }}" alt="logo ocio zgz" width="200px" height="120px">
                </a>
                <i class="fa fa-bars" id="hamburger" onclick="" style="font-size: 45px; cursor: pointer;"></i>
            </div>

            <div class="nav">
                <ul id="nav-events">
                    <a class="nav-title" href="/">HOME</a><br><br>
                    <span class="nav-title">EVENTOS</span>
                    <li><a href="/admin">Inicio</a></li>
                    <li><a href="/admin/eventos">Listado de eventos</a></li>
                    <li><a href="/admin/eventos/create">Nuevo evento</a></li>
                    <!-- <li><a href="#">Estadísticas</a></li> -->
                </ul><br><br>

                <ul id="nav-events">
                    <span class="nav-title">RESERVAS</span>
                    <li><a href='{{ route("total-reservas")}}'>Ver reservas</a></li>
                </ul><br><br>
                <!--Título del menú-->
                <ul id="nav-events">
                    <span class="nav-title">CONFIGURACIÓN</span>
                    <li><a href="{{route('config')}}">Mi perfil</a></li>
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Cerrar sesión') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul><br><br>
            </div>
        </aside>

        <!--MAIN-->
        <main style="max-height: 90%;">
            @yield('content')
        </main>
        <footer></footer>
    </div>
    <script type=" text/javascript" src="{{ asset('js/loader.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('js/toggleMenu.js') }}"></script> -->

</body>

</html>