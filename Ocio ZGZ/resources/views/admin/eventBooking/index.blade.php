@extends('admin.app')
@section('title', 'Lista de Reservas')
<!--Título dinámico-->
<style>
    table {
        width: 80%;
        border-collapse: collapse;
    }

    th,
    td {
        width: 25%;

        vertical-align: top;
        padding: 0.3em;
        caption-side: bottom;

        border-collapse: collapse;
    }

    td {
        text-align: center;
        white-space: nowrap;
        border-bottom: 1px solid black;
        padding: 1em;
    }

    thead {
        background: linear-gradient(to left, #ffc62a, #ff62c6);
        color: white;
        font-weight: bold;
    }

    th {
        text-align: center;
    }
</style>

@section('content')
<h1>Reservas para: {{$event->name}} | {{$event->date->format('d-m-Y')}}</h1>
<div>
    @if (!empty($bookings))
    <table>
        <thead>
        <tr>
                <th>NOMBRE</th>
                <th>ASISTENTES</th>
                <th>IMPORTE</th>
            </tr>
        </thead>
        @foreach ($bookings as $booking)
        <tr>
            <td>{{$booking->user->name}} {{$booking->user->surname}}</td>
            <td>{{$booking->asistentes}}</td>
            <td>{{$booking->price}}€</td>
        </tr>
        @endforeach
    </table>
    @else
    <p>No hay reservas para este evento</p>
    @endif
</div>

@endsection