@extends('client.app')
@section('title', 'Nuestros colaboradores')
<!--Título dinámico-->
<style>
    #container main {
        padding: 2em 0em 2em 0em;
    }

    .alert-message {
        width: 100%;
        position: fixed;
        bottom: 0;
        z-index: 888;
        height: 3em;
        background-color: rgb(181, 235, 204);
        border-bottom: 2px solid rgb(43, 159, 92);
        display: flex;
        text-align: center;
        align-items: center;
        color: rgb(43, 159, 92);
        /* border-radius: 10px; */
    }

    #all-events {
        display: grid;
        grid-template-columns: repeat(4, 1fr);
        grid-auto-flow: 100px;
        grid-gap: 1em;
        padding-top: 2em;
    }

    .card {
        background-color: white;
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.2);
        border-radius: 5px;
        /* overflow: hidden; */
        position: relative;
        /* height: 22em; */
        transition: all 0.3s;

    }

    .card:hover {
        transform: scale(1.06);

        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.4);
    }


    .card-container {
        overflow-y: auto;
        margin: auto;
        padding: 2em 1em 2em 1em;
        height: auto + 5em;
    }


    .barra {
        position: absolute;
        bottom: 0;
        width: 100%;
        height: 0.7em;
        background: linear-gradient(to left, #ffc62a, #ff62c6);
    }

    .container-avatar {
        width: 60px;
        height: 60px;
        border-radius: 900px;
        overflow: hidden;
        position: absolute;
        bottom: -25;
        margin-left: auto;
        margin-right: auto;
        left: 0;
        right: 0;

    }

    .container-avatar img {
        width: 100%;
        height: 100%;
    }

    @media screen and (min-width: 0px) and (max-width: 540px) {
        #all-events {
            grid-template-columns: repeat(1, 1fr);
        }
    }

    @media screen and (min-width: 541px) and (max-width: 768px) {
        #all-events {
            grid-template-columns: repeat(2, 1fr);
        }

        #logo a img {
            width: 70%;
            margin: 0;
        }
    }
</style>
<!-- @if(session('message')) -->
<div id="display">
    <div class="alert-message">
        <i class="fa fa-check" style="border: 2px solid rgb(43, 159, 92); border-radius:50%;padding:0.5em; margin-right:1em; margin-left:1em"></i><strong>{{session('message')}} - <a href="/mis-reservas" style="color: rgb(43, 159, 92);">Ver reservas</a></strong>
        <i class="fa fa-times" id="close-alert" style="position: absolute; right:20px; font-size:1.3em; cursor:pointer"></i>
    </div>
</div>
<!-- @endif -->
</div>


@section('content')
<!-- @include('admin.includes.alert-message')     -->
<div>
    <div style="display: flex; flex-direction:row; justify-content:space-between">
        <h1 style="width: 13em;">Nuestros colaboradores</h1>
        <!-- <div style="align-items:center; display:flex; flex-direction:row; ">
            <i class="fa fa-search" style="margin-right: 0.5em; font-size:1.1em"></i>
            <input type="text" name="colaboradores_filter" id="colaboradores_filter" style="border-radius: 20px; border: 1px solid black; padding:0.2em">
        </div> -->
    </div>
</div>
<div id="all-events">


    @if (!empty($colaboradores) )
    @foreach ($colaboradores as $colaborador)
    <div class="card">
        <a href="<?= 'usuario/' . $colaborador->id . '/eventos' ?>">
            <div style="position: relative;">
                <img src="{{url('user/background/'.$colaborador->background)}}" style="width:100%;" class="avatar">
                <div class="container-avatar">
                    <img src="{{url('user/avatar/'.$colaborador->image)}}" class="avatar">
                </div>
            </div>
            <div class="card-container" style="text-align: center; overflow-y:auto">
                <h3><b>{{$colaborador->nick}}</b></h3>
                <p style="text-align:left; margin-top: 10px">{{$colaborador->description}}</p>
            </div>
            <div class="barra"></div>
        </a>
    </div>
    @endforeach
    @else
    <div style="width: 100vw; color:tomato">
        <h3>Lo sentimos! Todavía no hay colaboradores</h3>
    </div>
    @endif
</div>
@endsection