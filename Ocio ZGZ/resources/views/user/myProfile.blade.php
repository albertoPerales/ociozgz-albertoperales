@extends('client.app')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function() {
    $('#close-alert').click(function() {
        $('.alert-message').css('display', 'none');
    });

});
</script>
@section('title', 'Editar perfil:'.' '.$user->nick)
<style>
    .box1 {
        background-color: #fff;
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.2);
        margin-top: 2em;
        padding: 2em;
        border-radius: 10px;
        transition: ease-in-out 0.3s;
        margin-right: 2em;
        width: 70%
    }

    .box2 {
        background-color: #fff;
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.2);
        margin-top: 2em;
        padding: 2em;
        border-radius: 10px;
        transition: ease-in-out 0.3s;
        height: fit-content;
        width: fit-content;
    }

    .box {
        background-color: #fff;
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.2);
        margin-top: 2em;
        padding: 2em;
        border-radius: 10px;
        transition: ease-in-out 0.3s;
    }

    #welcome {
        display: flex;
        flex-direction: column;
    }

    input[type="text"],
    input[type="password"] {
        background-color: rgb(229, 229, 229);
        height: 2.5em;
        border: none;
        padding: 10px;
    }

    form {
        width: 100%;
        margin-top: 1em;
    }

    .form {
        display: flex;
        flex-direction: column;
        margin-bottom: 1em;
    }

    .alert-message {
        width: 100%;
        height: 3em;
        background-color: rgb(181, 235, 204);
        border-bottom: 2px solid rgb(43, 159, 92);
        border-radius: 10px;
        padding: 1.2em;
        display: flex;
        text-align: center;
        align-items: center;
        margin-bottom: 1em;
        color: rgb(43, 159, 92);
    }

    .error-message {
        color: tomato;
        margin-top: 10px
    }


    label {
        margin-bottom: 10px;
    }

    .btn-form {
        width: 100%;
        background: linear-gradient(to left, #ffc62a, #ff62c6);
        color: #fff;
        text-align: center;
        border: none;
        padding: 1em;
        margin-bottom: 2em;
        font-size: 1em;
    }

    #container {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        width: 100%;

    }

    @media screen and (min-width: 0px) and (max-width: 768px){
        #container {
            flex-direction: column;
        }

        .box1, .box2 {
            width: 100%;
        }
    }

    @media screen and (min-width: 541px) and  {}
</style>


<!--Título dinámico-->
@section('content')
@include('admin.includes.alert-message')
<div id="welcome">
    <div style="width: 100%; height:fit-contents">
        <div style="text-align:center; display:flex; flex-direction:row;justify-content:center;  background-image: linear-gradient(90deg, #ffbb00, #ff137d), url('{{asset("https://images.pexels.com/photos/1238966/pexels-photo-1238966.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")}}');background-blend-mode: lighten;background-size: cover; height:fit-content; width:100%; padding-top:2em; padding-bottom: 2em;border-radius:20px;box-shadow: 0px 0px 20px -4px rgba(0,0,0,0.4)">
            @if (Auth::user()->image)
            <img src="{{url('user/avatar/'.Auth::user()->image)}}" style="margin-right:1em; height:100px; width:100px; border-radius:50%" class="current-avatar">
            @endif
            <div style="width: 10px;border-radius:10px;height:auto;background:white;margin-right:1em;"></div>
            <div style="text-align:left">
                <h1 style="color:white;">{{$user->name}} {{$user->surname}}</h1>
                <h2 style="color: white;">{{$user->nick}}</h2>
            </div>
        </div>
    </div>
    <div id="container">
        <div class="box1">
            <h1 style="width: fit-content;">Mis datos<div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 6px; width: 100%; margin-top: 10px;border-radius:20px"></div>
            </h1>
            <form action="{{route('user.update')}}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="form">
                    <label for="name">Nombre</label>
                    <input type="text" name="name" value="{{Auth::user()->name}}">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <p class="error-message"><strong>{{ $message }}</strong></p>
                    </span>
                    @enderror
                </div>
                <div class="form">
                    <label for="nick">Apellidos</label>
                    <input type="text" name="surname" value="{{Auth::user()->surname}}">
                    @error('surname')
                    <span class="invalid-feedback" role="alert">
                        <p class="error-message"><strong>{{ $message }}</strong></p>
                    </span>
                    @enderror
                </div>

                <div class="form">
                    <label for="nick">Nick público</label>
                    <input type="text" name="nick" value="{{Auth::user()->nick}}">
                    @error('nick')
                    <span class="invalid-feedback" role="alert">
                        <p class="error-message"><strong>{{ $message }}</strong></p>
                    </span>
                    @enderror
                </div>
                <div class="form">
                    <label for="email">Correo electrónico</label>
                    <input type="text" name="email" value="{{Auth::user()->email}}">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <p class="error-message"><strong>{{ $message }}</strong></p>
                    </span>
                    @enderror
                </div>
                @if (Auth::user()->role_id == 2)
                    <div class="form">
                        <label for="description">Descripción</label>
                        <input type="text" name="description" value="{{Auth::user()->description}}">
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                            <p class="error-message"><strong>{{ $message }}</strong></p>
                        </span>
                        @enderror
                    </div>
                @endif

                <div class="form">
                    <label for="image">Imagen de perfil</label>
                    @if (Auth::user()->image)
                    <img src="{{url('user/avatar/'.Auth::user()->image)}}" style="height:50px; width:50px" class="current-avatar">
                    @endif
                    <input type="file" name="image" value="{{Auth::user()->image}}" style="margin-top: 10px;">
                    @error('image')
                    <span class="invalid-feedback" role="alert">
                        <p class="error-message"><strong>{{ $message }}</strong></p>
                    </span>
                    @enderror
                </div>

                <div class="form">
                    <input class="btn-form" style=" cursor: pointer;" type="submit" value="Guardar">
                </div>
            </form>
        </div>
        <div class="box2">
            <h1 style="width: fit-content;">Cambiar contraseña<div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 6px; width: 100%; margin-top: 10px;border-radius:20px"></div>
            </h1>
            <form action="{{route('user.updatePassword')}}" method="post">
                @csrf
                <div class="form">
                    <label for="password">Nueva contraseña</label>
                    <input type="password" name="password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <p class="error-message"><strong>{{ $message }}</strong></p>
                    </span>
                    @enderror
                </div>
                <div class="form">
                    <label for="password_confirmation">Confirmar ontraseña</label>
                    <input type="password" name="password_confirmation">
                    @error('password_confirmation')
                    <span class="invalid-feedback" role="alert">
                        <p class="error-message"><strong>{{ $message }}</strong></p>
                    </span>
                    @enderror
                </div>
                <div class="form" style="margin-bottom: 0;">
                    <input class="btn-form" style=" cursor: pointer;" type="submit" value="Guardar">
                </div>
            </form>

        </div>
    </div>
</div>
@endsection