@extends('client.app')
@section('title', $event->name)
<!--Título dinámico-->
<style>
    .box {
        background-color: #fff;
        padding: 2em;
        border-radius: 5px;
        transition: ease-in-out 0.3s;
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.2);
    }
    #atras {
        transition: ease-in-out 0.2s;
        cursor: pointer;
    }
    #atras:hover {
        box-shadow: 0px 0px 20px -2px #ff62c6;
        transform: scale(1.05);
    }


    .welcome {
        display: flex;
        flex-direction: column;
        width: 100%;
        position: relative;

    }

    input[type="number"] {
        background-color: rgba(229, 229, 229, 0.5);
        height: 2em;
        border: none;
        padding: 10px;
        margin-left: 10px;


    }

    .flyer {
        transition: ease-in-out 0.3s;
        max-width: 200px;
        border-radius: 10px;
    }

    .flyer:hover {
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.3);
    }

    @media screen and (min-width: 0px) and (max-width: 540px) {
        main{
            overflow-x: auto;
        }
    }
   
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    $(document).ready(function() {

        $('#asistentes').on('change', function() {
            calcular();
        })
    });

    function calcular() {
        var asistentes = $('#asistentes').val();
        var precio_asistente = "<?= $event->price ?>"
        var price = asistentes * precio_asistente;

        if (asistentes >= 1 && asistentes <= <?= $event->capacity ?>) {
            $('#precio_total').html(price + '€');
        } else {
            $('#precio_total').html('No válido');
        }
    }
</script>
@section('content')
@if (($event->checkState() == true) && $event->state)
<h1>Evento: {{$event->name}}</h1>
<div class="welcome" style="margin-top: 2em;">
    <div style="display: flex; flex-direction: row;">
        <div style="margin-right: 1em;">
            <li>
                <div>
                    @if ($event->image_path)
                    <img class="flyer" style="max-width:200px;" src="{{url('event/flyer/'.$event->image_path)}}">
                    @endif
                </div>
            </li>
        </div>
        <div style="display: flex; flex-direction:column">
            <div class="box" style="position:relative; height:100%">
                <div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 20px; width: 100%;  position:absolute; bottom:0; left:0;"></div>
                <div style="white-space: nowrap;">
                    <!-- <i class=" fa fa-caret-left" style="font-size: 50px; position: absolute; left:-15; color:#fff"></i> -->
                    <h2 style="color: orange; margin-bottom: 10px">1. Información del evento</h2>
                </div>
                <ul>
                    <li><br>
                        <h2>{{$event->name}}</h2>
                    </li>
                    <li>
                        <h3>{{$event->date->format('d-m-Y')}}</h3><br>
                    </li>
                    <li style="display: flex; flex-direction:row; justify-content:space-between">
                        <p style="font-size: 1.2em;">Desde <span style="font-weight: bold;">{{$event->price}}€</span></p>
                        <p style="font-size: 1.2em;">Hasta las <span style="font-weight: bold;">22:00</span></p>
                    </li><br>
                    <li>
                        <p style="font-size: 1.1em;">{{$event->description}}</p>
                    </li>
                </ul>
            </div>
            <div style="margin-top: 1em;">
                <div style="display:flex;flex-direction:row;margin-left:20px; ">
                    <a href="<?= '/evento/' . $event->id . '/reservar' ?>" style="
                    width: 70%;
                    background: linear-gradient(to left, #ffc62a, #ff62c6);
                    color: #fff;
                    text-align: center;
                    border: none;
                    margin-top: 10px;
                    margin-right:10px;
                    padding:0.7em;
                    border-radius:20px;
                     padding-left:2em;
                     padding-right:2em;">Reservar
                    </a>
                    <a href="javascript:history.back()" style="
                    width: 30%;
                    background: linear-gradient(to left, #ffc62a, #ff62c6);
                    color: #fff;
                    text-align: center;
                    border: none;
                    margin-top: 10px;
                    padding:0.7em;
                    border-radius:20px;
                    padding-left:2em;
                    padding-right:2em;">Atrás
                    </a>
                </div>
            </div>
        </div>
       
        @else
            <div id="steps" style="display: flex; flex-direction:column; margin:auto; position:relative">
                <div id="info_panel" class="box" style="position:relative; height:100%;width:fit-content;">
                    <div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 20px; width: 100%;  position:absolute; bottom:0; left:0;"></div>
                    <div style="text-align: center;">
                        <h1 style="color: tomato;">Oops!</h1><br>
                        <h2>LO SENTIMOS, ESTE EVENTO YA NO ESTÁ ABIERTO</h2>
                        <img src="{{ asset('img/closed.gif') }}" alt="evento cerrado" />
                    </div>
                </div>
                <a id="atras" href="javascript:history.back()" style="
                    position:absolute;
                    right:0; bottom:-60;
                    width: 30%;
                    background: linear-gradient(to left, #ffc62a, #ff62c6);
                    color: #fff;
                    text-align: center;
                    border: none;
                    margin-top: 10px;
                    padding:0.7em;
                    border-radius:20px;
                    padding-left:2em;
                    padding-right:2em;">Atrás
                </a>
            </div>
        @endif
        @endsection