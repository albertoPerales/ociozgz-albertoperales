@extends('client.app')
@section('title', 'Eventos en '.$user->nick)
<style>
    #container main {
        padding: 2em 0em 2em 0em;
    }

    .card-container {
        background-color: white;
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.1);
        border-radius: 20px;
        padding: 1em;
        margin: auto;
        position: relative;
        overflow: hidden;
        max-width: 230px;
        transition: ease-in-out 0.2s;

    }

    .card-container:hover {
        transform: scale(1.04);
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.2);
    }

    #events {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        height: 70%;
        margin-bottom: 5em;
    }

    #all-events {
        display: grid;
        grid-template-columns: repeat(4, 1fr);
        grid-auto-flow: 100px;
        grid-gap: 1em;
        padding-top: 2em;
    }

    @media screen and (min-width: 0px) and (max-width: 540px) {
        #all-events {
            grid-template-columns: repeat(1, 1fr);

        }
    }

    @media screen and (min-width: 541px) and (max-width: 768px) {
        #all-events {
            grid-template-columns: repeat(2, 1fr);
        }
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    // $(document).ready(function(e) {
    //     $(".card-container").on('mouseover', function() {
    //         $(this).find('.container-info').show();
    //     });
    //     $(".card-container").on('mouseout', function () {
    //         $(this).find('.container-info').hide();
    //     });
    // });
</script>

@section('content')
<!-- @include('admin.includes.alert-message')     -->
<h1 style="width: auto;">Últimos eventos de {{$user->nick}}
</h1>
<div id="all-events">

    @if (!empty($events) )
        @foreach ($events as $event)
            @if (($event->checkState() == true) && $event->state)
            <a href="<?= '/evento/' . $event->id . '/reservar' ?>">
                <div class="card-container">
                    @if ($event->image_path)
                    <img style="max-width:200px; border-radius:20px;" src="{{url('event/flyer/'.$event->image_path)}}">
                    <div class="container-info" style="margin: auto; position:absolute; height:100%; width:100%; top:0;left:0;background-color:white; max-width:100%;padding:1em; display:none">
                        <div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 15px; width: 100%;  position:absolute; bottom:0; left:0"></div>
                        <div class="user-label" style="margin-bottom:1em;text-align:center; width:100%; padding:0.5em 0.7em 0.5em 0.7em; background: linear-gradient(to left, #ffc62a, #ff62c6); color: #fff; border-radius:20px">
                            @foreach ($users as $user)
                            @if ($event->user_id == $user->id)
                            {{$user->nick}}
                            @endif
                            @endforeach
                        </div>
                        <h2 style="font-weight: bold; font-size: 1.5em">{{$event->name}}</h2>
                        <h3 style="color: orange; margin-top: 7px">{{$event->date->format('d-m-Y')}}</h3>
                        <p style="margin-top: 1em; color: rgb(60, 60, 60); font-size: 0.9em; height:3.5em">{{$event->description}}</p>
                    </div>
                    @else
                    <div class="container-info">
                        <div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 15px; width: 100%;  position:absolute; bottom:0; left:0"></div>
                        <div class="user-label" style="margin-bottom:1em;text-align:center; width:100%; padding:0.5em 0.7em 0.5em 0.7em; background: linear-gradient(to left, #ffc62a, #ff62c6); color: #fff; border-radius:20px">
                            @foreach ($users as $user)
                            @if ($event->user_id == $user->id)
                            {{$user->nick}}
                            @endif
                            @endforeach
                        </div>
                        <h2 style="font-weight: bold; font-size: 1.5em">{{$event->name}}</h2>
                        <h3 style="color: orange; margin-top: 7px">{{$event->date->format('d-m-Y')}}</h3>
                        <p style="margin-top: 1em; color: rgb(60, 60, 60); font-size: 0.9em; height:3.5em">{{$event->description}}</p>
                    </div>
                    @endif
                </div>
            </a>
            @endif
        @endforeach
    @else
    <h4 style="background: rgb(250, 229, 233); color:rgb(254, 97,111); padding:1em; white-space:nowrap; border-radius:10px">No hay eventos disponibles</h4>
    @endif
</div>
@endsection