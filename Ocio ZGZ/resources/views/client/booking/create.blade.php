@extends('client.app')
@section('title', $event->name)
<!--Título dinámico-->
<style>
    input[type="submit"]:hover,
    #atras:hover {
        box-shadow: 0px 0px 20px -2px #ff62c6;
        transform: scale(1.05);
    }

    input[type="submit"], #atras {
        transition: ease-in-out 0.2s;
        cursor: pointer;
    }

    .box {
        background-color: #fff;
        padding: 2em;
        border-radius: 5px;
        transition: ease-in-out 0.3s;
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.2);
    }

    .welcome {
        display: flex;
        flex-direction: column;
        width: 100%;
        position: relative;

    }

    input[type="number"] {
        background-color: rgba(229, 229, 229, 0.5);
        height: 2em;
        border: none;
        padding: 10px;
        margin-left: 10px;
    }

    .flyer {
        transition: ease-in-out 0.3s;
        max-width: 200px;
        border-radius: 10px;
    }

    .flyer:hover {
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.3);
    }
    main{
        overflow-x: auto;
    }

    #container {
        display: flex;
        flex-direction: row;
        
    }

    .content-container {
        display: flex;
        flex-direction: row;
        margin-top: 2em
    }

    #precio-panel {
        margin-left: 20px;
        height: 17em;
        position: relative;
    }

    @media screen and (min-width: 0px) and (max-width: 768px) {
        #container {
            flex-direction: column;
            justify-content: center;
            flex-wrap: wrap;
            margin: auto;
        }

        #container li:first-child {
            text-align: center;
        }

        #precio-panel {
            margin-top: 1em;
            margin-left: 0;
        }

        .content-container {
            flex-direction: column;
        }
    }


</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    $(document).ready(function() {

        $('#asistentes').on('change', function() {
            calcular();
        })
    });
    $('#steps').steps();

    function calcular() {
        var asistentes = $('#asistentes').val();
        var precio_asistente = "<?= $event->price ?>"
        var price = (asistentes * precio_asistente) + 1;

        if (asistentes >= 1 && asistentes <= <?= $event->restantes ?>) {
            $('#precio_total').html(price + '€');
        } else {
            $('#precio_total').html('No válido');
        }
    }
</script>
@section('content')
<!--Si el evento está disponible-->
@if (($event->checkState() == true) && $event->state)
<h1>Evento: {{$event->name}}</h1>
<div class="welcome" style="margin-top: 2em;">
    <div id="container">
        <div style="margin-right: 1em;margin-top: 2em">
            <li>
                @if ($event->image_path)
                <img class="flyer" style="max-width:200px;" src="{{url('event/flyer/'.$event->image_path)}}">
                @endif
            </li>
        </div>
        <div class="content-container">
            <div id="steps" style="display: flex; flex-direction:column">
                <div id="info_panel" class="box" style="position:relative; height:100%">
                    <div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 20px; width: 100%;  position:absolute; bottom:0; left:0;"></div>
                    <div style="white-space: nowrap;">
                        <!-- <i class=" fa fa-caret-left" style="font-size: 50px; position: absolute; left:-15; color:#fff"></i> -->
                        <h2 style="color: orange; margin-bottom: 10px">1. Información del evento</h2>
                    </div>
                    <ul>
                        <li><br>
                            <h2>{{$event->name}}</h2>
                        </li>
                        <li>
                            <h3>{{$event->date->format('d-m-Y')}}</h3><br>
                        </li>
                        <li style="display: flex; flex-direction:row; justify-content:space-between">
                            <p style="font-size: 1.2em;">Desde <span style="font-weight: bold;">{{$event->price}}€</span></p>
                            <p style="font-size: 1.2em;">Hasta las <span style="font-weight: bold;">{{$event->max_hour->format('H:i')}}h</span></p>
                        </li><br>
                        <li>
                            <p style="font-size: 1.1em;">{{$event->description}}</p>
                        </li>
                    </ul>
                </div>
            </div>
            <form id="info_reserva" action="{{url('/evento/'.$event->id.'/reservar')}}" method="post">
                @csrf
                <div class="box" id="precio-panel">
                    <div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 20px; width: 100%;  position:absolute; bottom:0; left:0;"></div>
                    <div>
                        <div style="white-space: nowrap;">
                            <!-- <i class=" fa fa-caret-left" style="font-size: 50px; position: absolute; left:-15; color:#fff"></i> -->
                            <h2 style="color: orange; margin-bottom: 10px">2. Reserva</h2><br>
                        </div>
                        <li>
                            <h3 style="white-space: nowrap;">Número de asistentes: <input type="number" value="1" id="asistentes" name="asistentes" min="1" max="<?= $event->restantes ?>"> </h3>
                            <p style="font-size: 0.9em;margin-top:7px">(El precio por asistente es de <span id="precio_asistente"><?= $event->price ?></span>€)</p>
                            <p style="font-size: 0.9em;margin-top:7px">(Máximo de asistentes: <span id="precio_asistente"><?= $event->restantes ?></span>)</p>
                        </li>
                        <li style="margin-top:1em;">
                            <h3>TOTAL:<span id="precio_total" style="margin-left: 10px;">{{($event->price)+1}}€</span></h3>
                            <p>(incluye 1€ de gastos de gestión)</p>
                        </li>
                    </div>
                </div>
                <div style="margin-top: 1em;">
                    <div style="display:flex;flex-direction:row;margin-left:20px; ">
                        <input type="submit" value="Reservar" style="
                    font-size:1em;
                    width: 70%;
                    background: linear-gradient(to left, #ffc62a, #ff62c6);
                    color: #fff;
                    text-align: center;
                    border: none;
                    margin-top: 10px;
                    margin-right:10px;
                    padding:0.7em;
                    border-radius:20px;
                     padding-left:2em;
                     padding-right:2em;">
                        </input>
                        <a id="atras" href="javascript:history.back()" style="
                    width: 30%;
                    background: linear-gradient(to left, #ffc62a, #ff62c6);
                    color: #fff;
                    text-align: center;
                    border: none;
                    margin-top: 10px;
                    padding:0.7em;
                    border-radius:20px;
                    padding-left:2em;
                    padding-right:2em;">Atrás
                        </a>
                    </div>
                </div>
            </form>
        </div>
        @else
        <!--Si el evento no está disponible-->
        <div id="steps" style="display: flex; flex-direction:column; margin:auto">
            <div id="info_panel" class="box" style="position:relative; height:100%;width:fit-content;">
                <div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 20px; width: 100%;  position:absolute; bottom:0; left:0;"></div>
                <div style="text-align: center;">
                    <h1 style="color: tomato;">Oops!</h1><br>
                    <h2>LO SENTIMOS, ESTE EVENTO YA NO ESTÁ ABIERTO</h2>
                    <img src="{{ asset('img/closed.gif') }}" alt="evento cerrado" />
                </div>

            </div>
        </div>


        @endif


        @endsection