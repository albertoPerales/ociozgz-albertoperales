@extends('client.app')
@section('title', 'Contacta con nosotros')
<style>
    input[type="text"] {
        background-color: rgba(229, 229, 229, 0.5);
        border: none;
        padding: 10px;
        /* width:; */
    }

    .form {
        margin: 20px;
        margin-top: 0;
        margin-left: 0;
        display: flex;
        flex-direction: column;
    }

    input[type="submit"] {
        margin-top: 1em;
        padding: 1em 3em 1em 3em;
        border-radius: 20px;
        border: none;
        cursor: pointer;
        background: linear-gradient(to left, #ffc62a, #ff62c6);
        color: white;
        transition: ease-in-out 0.3s;
    }
    .error-message {
        color: tomato;
        margin-top: 10px
    }


    input[type="submit"]:hover {
        box-shadow: 0px 0px 20px -2px #ff62c6;
        transform: scale(1.05);
    }

    .box {
        background-color: #fff;
        padding: 2em;
        border-radius: 5px;
        transition: ease-in-out 0.3s;
        width: 50%;
        margin-top: 2em;
        float: right
            /* box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.2); */
    }

    .img-container {
        width: 50%;
        float: left
    }

    .alert-message {
        width: 100%;
        height: 3em;
        background-color: rgb(181, 235, 204);
        border-bottom: 2px solid rgb(43, 159, 92);
        display: flex;
        text-align: center;
        align-items: center;
        margin-bottom: 1em;
        color: rgb(43, 159, 92);
        border-radius: 10px;
    }

    #content {
        display: flex;
        flex-direction: row;
        align-items: center;
    }



    @media screen and (min-width: 0px) and (max-width: 540px) {
        #content {
            flex-direction: column;
        }

        div.box {
            padding: 0.5em;
            width: 100%;
        }

        .img-container {
            width: 100%;
        }
    }

    @media screen and (min-width: 541px) and (max-width: 768px) {
        #content {
            flex-direction: column;
        }

        div.box {
            padding: 0.5em;
            width: 100%;
        }

        .img-container {
            width: 70%;
        }
    }
</style>
@section('content')
<div style="position: relative">
    @include('admin.includes.alert-message')
    <div style="width: 100%; margin-right:2em">
        <h1 style="text-align: center;">Contacta con nosotros</h1><br>
        <p style="font-size: 1.1em;">¿Tienes algún problema técnico, sugerencia o duda acerca de nosotros? ¿Eres dueño de un establecimiento y te gustaría colaborar con nostros? Rellena el formulario y estaremos encantados de ayudarte </p>
        <div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 6px; width: 100%; margin-top: 2em; border-radius:20px; margin:2em auto"></div>
    </div><br>
    <div id="content">
        <div class="img-container">
            <img src="{{asset('img/contacto.jpg')}}" width="100%">
        </div>
        <div class="box">
            <form method="POST" action="{{route('contactUsPost')}}">
                @csrf
                <div class="form">
                    <label for="name">Nombre y apellidos</label><br>
                    <input required type="text" name="name" placeholder="Introduce nombre y apellidos">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <p class="error-message"><strong>{{ $message }}</strong></p>
                    </span>
                    @enderror
                </div>
                <div class="form">
                    <label for="subject">Asunto</label><br>
                    <input required type="text" name="subject" placeholder="Introduce tu correo electrónico">
                    @error('subject')
                    <span class="invalid-feedback" role="alert">
                        <p class="error-message"><strong>{{ $message }}</strong></p>
                    </span>
                    @enderror
                </div>
                <div class="form">
                    <label for="email">Email</label><br>
                    <input required type="text" name="email" placeholder="Introduce tu correo electrónico">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <p class="error-message"><strong>{{ $message }}</strong></p>
                    </span>
                    @enderror
                </div>
                <div class="form">
                    <label for="msg">Mensaje</label><br>
                    <textarea maxlength="2000" style="max-height:20em;border:none;font-family:'Montserrat';min-width:100%;background:rgba(229, 229, 229, 0.5);max-width:100%;
                    padding: 10px;" name="msg">
                    </textarea>
                    @error('msg')
                    <span class="invalid-feedback" role="alert">
                        <p class="error-message"><strong>{{ $message }}</strong></p>
                    </span>
                    @enderror
                </div>
                <input type="submit" value="Enviar">
            </form>
        </div>
    </div>

</div>

@endsection