@extends('client.app')
@section('title', 'Home')

@section('banner')
<!--BANNER-->
<div id="banner">
        <div id="content-banner">
          <h1>HAZ TUS RESERVAS DE FORMA INMEDIATA EN:</h1>
          <h2>Discotecas, salas, bares, restaurantes...</h2>
          <!-- <div id="btn-banner"><a href="/colaboradores" style="margin-bottom:2em">Ver eventos</a></div> -->
          
        </div>
        <svg id="wave" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path class="wave" fill="#fff" fill-opacity="1"
            d="M0,192L48,197.3C96,203,192,213,288,229.3C384,245,480,267,576,250.7C672,235,768,181,864,181.3C960,181,1056,235,1152,234.7C1248,235,1344,181,1392,154.7L1440,128L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z">
          </path>
        </svg>
      </div>
    </header>
@endsection

<!--Título dinámico-->
@section('content')
<!--MAIN-->
<!--ARTÍCULO2-->
<article id="article2">
  <h1>Realiza tus reservas<br> <span>de forma rápida y sencilla</span></h1>
  <div id="cols">
    <div id="col1">
      <i class="fa fa-search"></i>
      <p>Encuentra tu local favorito entre nuestros colaboradores y sé de los primeros en reservar para sus eventos</p>
    </div>

    <div id="col2">
      <i class="fa fa-check"></i>
      <p>Busca eventos entre nuestros colaboradores y obtén múltiples ventajas como promociones y detalles de estos. ¡No te pierdas ningún detalle!</p>
    </div>
    <div id="col3">
      <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
      <p>Avisa a tus amigos y confirma tu reserva, no esperes hasta el último momento, ¡las entradas vuelan!</p>
    </div>
  </div>
</article>

<!--ARTÍCULO3-->
<article id="sponsors">
  <h1>Nuestros colaboradores</h1>
  <p>Ocio ZGZ cuenta con un amplio número de locales en el sector de la hostelería, tanto bares o restaurantes como discotecas o locales de ocio nocturno.</p>
  <p>Ofrecemos una unión de todos estos locales bajo una misma comunidad, de tal forma que se facilite el crecimiento de estos tanto en la era COVID como tras esta.</p>
</article>

@endsection