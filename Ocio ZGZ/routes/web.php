<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\UserEventBookingController;
use App\Http\Controllers\ContactUsController;


//Auth
Auth::routes(['verify' => true]);


// Images
    // Get Flyer
    Route::get('/event/flyer/{filename}', [EventController::class, 'getImage'])->name('event.flyer');
    // Get avatar
    Route::get('/user/avatar/{filename}', [UserController::class, 'getImage'])->name('user.avatar');
    // Get bakground (encargado)
    Route::get('/user/background/{filename}', [UserController::class, 'getBackground'])->name('user.background');
//


// Storage user data from forms (edit profiles)
Route::middleware('auth','verified')->group(function () {
    Route::post('/user/update', [UserController::class, 'update'])->name('user.update');
    Route::post('/user/updatePassword', [UserController::class, 'updatePassword'])->name('user.updatePassword');
});
//


// Routes with no middlewares
Route::get('/', [HomeController::class, 'client_home'])->name('home');
Route::get('/colaboradores', [UserController::class, 'colaboradores'])->name('colaboradores');
Route::get('/contacta-con-nosotros', [ContactUsController::class, 'contactUs'])->name('contactUs');
Route::post('/contacta-con-nosotros', [ContactUsController::class, 'contactUsPost'])->name('contactUsPost');
//

/* --- CLIENT --- */ 
// Show all events of an user
Route::get('/usuario/{user_id}/eventos', [UserEventBookingController::class, 'showEvents'])->name('client.event.index');
Route::middleware('auth','verified')->group(function () {
    // Show all bookings I've made (as an user)
    Route::get('/mis-reservas', [BookingController::class, 'myBookings'])->name('listBookings');


    // Edit my profile
    Route::get('/user/editar-perfil', [UserController::class, 'myProfile'])->name('myProfile');

    // View details of specific events (it's not visible for users at first sight but through buttons)
    Route::get('/evento/{id}', [EventController::class, 'show'])->name('client.event.show');

    // Create a booking & store it
    Route::get('/evento/{id}/reservar', [BookingController::class, 'create'])->name('client.booking.create')->middleware('auth');
    Route::post('/evento/{id}/reservar', [BookingController::class, 'store'])->name('client.booking.store')->middleware('auth'); // Store booking in DB
});


/* --- ADMIN PANEL --- */ 
Route::prefix('admin')->middleware(['auth', 'admin', 'verified'])->group(function (){
    Route::get('/', [HomeController::class, 'admin_home'])->name('welcome'); // /admin
    Route::get('/total-reservas', [UserEventBookingController::class, 'index'])->name('total-reservas'); // /admin/mis-eventos
    Route::resource('eventos', EventController::class); // /admin/eventos
    Route::resource('reservas', BookingController::class); // /admin/reservas
    Route::get('/evento/{id}/reservas', [EventController::class, 'evento_reservas'])->name('evento_reservas');
    Route::get('/my-profile', [UserController::class, 'myProfile'])->name('config');
});
/*--------------------*/



