<p align="center"><a href="https://www.linkedin.com/in/alberto-perales-lafuente-bb11231aa/" target="_blank"><img src="https://i.ibb.co/pwQb5fg/logo.png" width="300"></a></p>

<!-- <p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p> -->

## Sobre Ocio Zgz

Ocio ZGZ es una aplicación web destinada a la gestión de eventos y reservas para el sector de la hostelería en España.
Esta aplicación ha sido desarrollada por Alberto Perales Lafuente como trabajo de fin de grado para el técnico superior de Desarrollo de Aplicaciones Web del instituto I.E.S Santiago 
Hernández (Junio 2021)

La aplicación se ha desarrollado en:
* Frontend: HTML5, CSS3, SASS, jQuery (JavaScript).
* Backend: Laravel (PHP).
* BBDD: MySQL.

