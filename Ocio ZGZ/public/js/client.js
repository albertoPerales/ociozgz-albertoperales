$(document).ready(function() {
    toggleMenu();
    goUp();
    loader();
});

// Botón ir arriba
function goUp() {
    $('#go-up').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 500)
    });
    $(window).scroll(function() {
        if ($(this).scrollTop(), 200) {
            $('#go-up').fadeIn();
        } else {
            $('#go-up').fadeOut();
        }
    });
}

// Script para cerrar alertas
$('#close-alert').click(function() {
    $('.alert-message').css('display', 'none');
});

const MOVIL = 1
const TABLET = 2
const ESCRITORIO = 3
var tipo = 0;

function main() {
    atarEvento();
}

function atarEvento() {
    window.addEventListener("resize", redimensionaVentana, false);
}

function redimensionaVentana() {
    var w = window.innerWidth;
    var h = window.innerHeight;
    // document.getElementById("demo").innerHTML = "Width: " + w + "<br>Height: " + h;
    if (w < 540 && tipo != MOVIL) {
        tipo = MOVIL;

        $('#xmenu').hide();
        $('#xmenu').css('display', 'none');
        // alert("Se ha cambiado a modalidad MOVIL");
    } else {
        $('#xmenu').show();
        $('#xmenu').css('display', 'flex');
        // alert('se ha cambiado a otra modalidad');
    }
}

function toggleMenu() {
    $("#hamburger").click(function() {
       
        $('#xmenu').slideToggle();
    });
}