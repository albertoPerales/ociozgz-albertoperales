<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Event;

class EventSeeder extends Seeder
{
    public function run()
    {
        Event::create([
            'id' => '1',
            'user_id' => 2,
            'date' => '2021-07-20',
            'max_hour' => '22:00',
            'name' => 'Sábado 27 de Marzo',
            'description' => 'Celebramos la nueva imagen de Sala Campus',
            'location' => 'Calle Maestro Luna, 7, 50003 Zaragoza',
            'image_path' => 'flyer.jpg',
            'capacity' => 40,
            'restantes' => 40,
            'state' => 1, // 1 abierto, 0 cerrado
            'price' => 10
        ]);


    }
}
