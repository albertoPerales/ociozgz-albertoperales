<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'id' => '1',
            'role_id' => '2', //Encargado
            'name' => 'Eduardo',
            'surname' => 'Manos Tijeras',
            'nick' => 'Sala Campus',
            'email' => 'salacampus@gmail.com',
            'password' => bcrypt('secret'),
            'description' => 'Situada en pleno corazón del Casco Histórico de Zaragoza, referente entre el  público joven y universitario. Cada fin de semana una fiesta temática diferente, contamos con un gran equipo de  animación, show’s y decoración especial.'
        ]);

        User::create([
            'id' => '2',
            'role_id' => '2', //Encargado
            'name' => 'Samuel',
            'surname' => 'Trenzas Rastas',
            'nick' => 'Bocatart',
            'email' => 'bocatart@gmail.com',
            'password' => bcrypt('secret'),
            'description' => 'Bocadillos con Arte. En Nuestros Restaurantes Cocinamos con productos naturales y frescos de la tierra. Te esperamos en nuestros locales.'
        ]);

        User::create([
            'id' => '3',
            'role_id' => '2', //Encargado
            'name' => 'Joaquin',
            'surname' => 'Piripi',
            'nick' => 'Parros',
            'email' => 'parros@gmail.com',
            'password' => bcrypt('secret'),
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nibh nibh, pellentesque sit amet ipsum ornare.'
        ]);

        User::create([
            'id' => '7',
            'role_id' => '2', //Encargado
            'name' => 'Antonio',
            'surname' => 'Wilson',
            'nick' => 'Tony Wilson',
            'email' => 'tonywilson@gmail.com',
            'password' => bcrypt('secret'),
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nibh nibh, pellentesque sit amet ipsum ornare.'
        ]);

        User::create([
            'id' => '4',
            'role_id' => '1', //Usuario
            'name' => 'Cristina',
            'surname' => 'Perales Lafuente',
            'nick' => 'crispy',
            'email' => 'crispy@gmail.com',
            'password' => bcrypt('secret'),
        ]);

        User::create([
            'id' => '5',
            'role_id' => '1', //Usuario
            'name' => 'Rafa',
            'surname' => 'Cabeza',
            'nick' => 'rafaCabeza',
            'email' => 'rafacabeza@gmail.com',
            'password' => bcrypt('secret'),
        ]);

        User::create([
            'id' => '6',
            'role_id' => '1', //Usuario
            'name' => 'Yolanda',
            'surname' => 'Escorza',
            'nick' => 'yolandaEscorza',
            'email' => 'yolandaescorza@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
